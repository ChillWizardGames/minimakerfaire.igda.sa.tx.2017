# README #
### What is this repository for? ###

Here we have a ***Pong*** Example to help you get the understanding of fundamental Unity concepts.

### Where do I start? ###

This folder is the *root* directory for our Unity3D Project!
Here you can find the following folders and what they do.

### What do I need to start editing the source? ###

*A computer with a copy of Unity3D.*
Here we will be using version 5.5
If you are using a newer copy the concepts *should* still hold true.
Check below under *How do I get set up* to learn how to install Unity3D.

### How do I get set up? ###
Go to this link and download a version of Unity that is compatible with your Operation System
-> https://unity3d.com/get-unity/download
