﻿/*
///
    Sound Manager for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public static SoundManager Get;

    public AudioSource Source;

    public AudioClip FX_HitPaddle;
    public AudioClip FX_HitWall;

    public AudioClip FX_GameStart;
    public AudioClip FX_GameEnd;

    void Awake()
    {
        Get = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    public void Play( AudioClip sound )
    {
        Source.clip = sound;
        Source.Play();
    }
}
