﻿/*
///
    Goal for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour {

    public bool playerGoal = false;

    void Start () {
		
	}
	
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Ball")
        {

            if (UserManager.Get.Enemy.Speed < 15)
            {
                UserManager.Get.Enemy.Speed++;
            }

            if (!playerGoal)
            {
                ScoreManager.Get.IncrementScore(UserManager.Get.Player);
            }
            else
            {
                ScoreManager.Get.IncrementScore(UserManager.Get.Enemy);

                UserManager.Get.Enemy.X_LINE_OF_SIGHT -= .25f;
                UserManager.Get.Enemy.Speed+= .5f;
            }

            Ball.Get.Reset();

        }

    }
}
