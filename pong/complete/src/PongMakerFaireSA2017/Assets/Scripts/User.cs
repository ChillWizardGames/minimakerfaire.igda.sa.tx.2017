﻿/*
///
    User for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour {

    /*
        Speed   =   # Sets the amount of world space the player travels when moving
                    #
                    # *public*  - to edit this value in the Inspector
                    # *float*   - to have a finer range of movement. ( int would produce blocky movement )
                    #
                    # Values found here were found with trial and error aka "Gameplay Testing"
                    # We played the game until we found something comfortable, challenging, and fun!

        Score   =   # Tracks the current score; Both 'Player' and 'Enemy' Objects need their tracking variable.
                    #
                    # *public*  - to have other scripts edit these values.
                    # *int*     - only whole numbers needed; We have no need to have .5 of a goal.
                    #
                    # Values here start off at 0 and are increased by the Goal.cs script

    */

    public static User Get;

    public int Score;
    public float Speed;

    public bool IsAI;

    void Awake () { Get = this; }

}
