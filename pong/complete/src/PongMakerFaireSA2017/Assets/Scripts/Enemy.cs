﻿/*
///
    Enemy for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : User
{

    /*
    ///
        Here are the variables we are using to Model the *Enemy*

        First we will go over some key terms

            Access Modifier - # Used to tell different pieces of an app what can communicate

                public  - # Means that we can access these variables if there is a reference to this class.

            Date Type - # Used to tell the difference between objects and variables

                float       - # numbers with decimal points ( 3.14, 1.3, 4.2 )

        Now how do we use these pieces in Enemy.cs?

                DistanceFromBall    -   # A float tracking the ball's current distance fom this *Enemy* obj
                                        # Available for viewing in the Inspector with Public.
                                        # Set in 'GetAxisDistance'

    ///
    */

    const float X_POSITION      = 6.80f;
    public float X_LINE_OF_SIGHT = 7.25f;
    const float Y_LINE_OF_SIGHT =  .75f;

    public static Enemy Get;
    public float DistanceFromBall;

/*
///

    *Start* configures our *Enemy* Script

///
*/
    void Start () {

// Sets the IsAI boolean to true
        IsAI = true;

// Starts the Coroutine that checks the balls position and moves the enemy accordingly.

	}

    /*
    ///

        *MovementCheck* has all the A.I. behaviour allowing the Enemy to react to the *Ball*

            bool    - # used to track if something is either (True) or (False)
            Vector3 - # used to keep track of 3 floats (x,y,z)

            Now how do we use these pieces in 'MovementCheck'?

                _ballPosition    -   # A Vector3 that is used to store a snapshot of the Ball's Current position

                _thisPosition    -   # A Vector3 that is used to store a snapshot of this Enemy's position

    ///
    */
    public IEnumerator MovementCheck()
    {

        Vector3 _thisPosition = Vector3.zero;
        Vector3 _ballPosition = Vector3.zero;

        while ( StateHandler.Get.CurrentState == GameState.GameRunning )
        {

 // Takes a snapshot of this *Enemy* Obj position, as well as, the *Ball* position
            _thisPosition = this.transform.position;
            _ballPosition = Ball.Get.transform.position;

 // Checks if the *Ball* is in this *Enemy* Obj's LINE OF SIGHT on the X axis
            if      ( GetAxisDistance( _ballPosition.x, _thisPosition.x ) < X_LINE_OF_SIGHT )
            {
 // Checks if the *Ball* is in this *Enemy* Obj's LINE OF SIGHT on the Y axis

                if ( GetAxisDistance( _ballPosition.y, _thisPosition.y ) > Y_LINE_OF_SIGHT )
                {                   
 // If both of these are true then begin to move this *Enemy* Obj
 // We maintain the X Position in a const
 // By taking ( Time.deltaTime * Speed ) we calculate how much time has passed and how fast the *Enemy* is
 // Using Lerp we smooth out the movement of the *Enemy*
                    this.transform.position = new Vector3(  X_POSITION,
                                                            Mathf.Lerp( _thisPosition.y, _ballPosition.y, ( Time.deltaTime * Speed) ), 0);
                }
            }
// This Method will wait the following time to execute again...
            yield return new WaitForSeconds(.014f);
        }
    }

/*
///

    *GetAxisDistance* calculate the distance in world space from two positions

        First we take the position of *Ball* and subtract the value of this position

///
*/
    public float GetAxisDistance(float posBall, float posThis)
    {
        float _dis = Mathf.Sqrt(
                    Mathf.Pow( posBall - posThis, 2) );

        DistanceFromBall = _dis;
        return _dis;
    }

    void Awake () { Get = this; }

}
