﻿/*
///
    Menu Manager for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager Get;

    public GameObject PanelGameOver;

    public GameObject PanelCredit;
    public GameObject PanelMain;

    // Use this for initialization
    void Awake () {
        Get = this;
	}
	
}
