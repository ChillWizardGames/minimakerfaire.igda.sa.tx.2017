﻿/*
///
    Score Manager Script for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    /*
    ///
        Here are the variables we are using to Model the Score Manager.

        First we will go over some key terms

            Access Modifier - Used to tell different pieces of an app what can communicate

                public static   - # Means that we can access this reference from anywhere in the app
                const           - # these are hard coded into the application; Used for things which shouldn't change.
            
            Date Type - Used to tell the difference between objects and variables

                int         - # whole number integers ( 1, 2, 3 )
                Text        - # the default object that displays UI Text in Unity

        Now how do we use this in ScoreManager.cs?

        ScoreManager Get    -  # provides a way to Access this Class from anywhere allowing tremendous flexibility in development.

        void Awake          - # Sets the Get reference

        const int GOAL      - # Hard coded value for the amount of Goals ( Score ) Needed to Win!

        public Text Display - # Our Text Obj; The way we display the Score Text.
    ///
    */

    const int GOAL = 3;
 
    public static ScoreManager Get;

    public Text Display;
    public Text GameOverMessage;

    void Awake () { Get = this; }

/*
///
    Reset - # Restarts the Player / Enemy Score & Updates the Display
///
*/
    public void Reset ()
    {
        UserManager.Get.Player.Score = 0;
        UserManager.Get.Enemy.Score = 0;

        UpdateDisplay();
    }

/*
///
    IncrementScore - # Adds +1 to a user that has scored. Updates the display and Checks if a player has Hit the Required Amount of Goals
///
*/
    public void IncrementScore ( User target)
    {

        bool _isOver = false;

        target.Score++;

        UpdateDisplay();

// # Checks if either the Player / Enemy have scored the required # of GOALs
        if (UserManager.Get.Player.Score >= GOAL )
        {
            // # Changes the State of the Game to 'GameOver'
            _isOver = true;

            GameOverMessage.text = "Congrats, Player!";

        }
        if ( UserManager.Get.Enemy.Score >= GOAL)
        {
            _isOver = true;
            GameOverMessage.text = "Aww boo. Try Again :D";

        }

        // # Changes the State of the Game to 'GameOver'
        if (_isOver == true)
        {

            StateHandler.Get.ChangeState(GameState.GameOver);

        }
        else
        {
            // # Starts a new Timer for another Ball
            StartCoroutine(Ball.Get.SpawnTimer());
        }

    }

/*
///
    UpdateDisplay - # Updates the Text in the Display UI Text Element
///
*/
    public void UpdateDisplay (  )
    {
// Shows the player Score, followed by a gap, and then the Enemy Score.
        Display.text = UserManager.Get.Player.Score + "     " + UserManager.Get.Enemy.Score;
        
    }
}
