﻿/*
///
    Game Script for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
///
    Here are the variables we are using to Model the Game.

    First we will go over some key terms

        Access Modifier     - # Used to tell different pieces of an app what can communicate

            public static   - # Means that we can access this reference from anywhere in the app

     
    Now how do we use this in Game.cs?

    GameScript Get  - # provides a way to Access this Class from anywhere allowing tremendous flexibility in development.

    void Awake      - # Sets the Get reference

///
*/

public class GameScript : MonoBehaviour {

    public static GameScript Get;

    void Awake() { Get = this; }
    
/*
///
    Begin - # launches all of the crital game functions.
///
*/
    public void Begin ()
    {
// First we set the state of the game to Running
        StateHandler.Get.ChangeState(GameState.GameRunning);

// Next we enable movement of the Player and the Enemy Objects
        StartCoroutine(Player.Get.MovementCheck());
        StartCoroutine(Enemy.Get.MovementCheck());

// To Ensure a clean slate we Reset the Score
        ScoreManager.Get.Reset();

// To configure the Ball we enable it
// Reset it...
// Enable the Movement
// and Finally Start a Timer

        Ball.Get.gameObject.SetActive(true);
        Ball.Get.Reset();

        SoundManager.Get.Play(SoundManager.Get.FX_GameStart);

        StartCoroutine(Ball.Get.MovementCheck());
        StartCoroutine(Ball.Get.SpawnTimer());
    }

/*
///
    Over - # ends all of the crital game functions.
///
*/
    public void Over()
    {

        SoundManager.Get.Play(SoundManager.Get.FX_GameEnd);

        // We shut down the movement functions with StopAllCoroutines...

        Ball.Get.Animate.speed = 0;
        StopAllCoroutines();

        // We shut down the Ball by disabling it
        
        Ball.Get.gameObject.SetActive(false);
        

// Finally a Game Over Screen Pops Up
        MenuManager.Get.PanelGameOver.SetActive(true);
    }
}
