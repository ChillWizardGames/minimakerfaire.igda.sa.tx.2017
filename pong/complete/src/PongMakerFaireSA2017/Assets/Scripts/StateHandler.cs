﻿/*
///
    State Handler for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState
{
    MenuMain,
    MenuCredit,

    GameBegin,
    GameReset,
    GameRunning,
    GameOver
}

public class StateHandler : MonoBehaviour {

    public GameState CurrentState = GameState.MenuMain;

    public bool overState = false;
    public bool beginState = false;
    public Text winner;



    public static StateHandler Get;
	
    void Awake ()
    {
        Get = this;
    }

	void Start () {

        

	}

    public void ChangeState ( GameState value )
    {

        CurrentState = value;

        switch ( CurrentState )
        {
            case GameState.GameBegin:

                GameScript.Get.Begin();
                MenuManager.Get.PanelMain.SetActive(false);
                break;

            case GameState.GameRunning:
                

                break;
            case GameState.GameOver:

                GameScript.Get.Over();
                break;

            case GameState.MenuCredit:

                MenuManager.Get.PanelCredit.SetActive(true);
                break;

            case GameState.MenuMain:

                MenuManager.Get.PanelCredit.SetActive(false);
                MenuManager.Get.PanelMain.SetActive(true);
                break;
        }

    }
	
    public void Btn_GameReset()
    {

        GameScript.Get.Begin();

        MenuManager.Get.PanelGameOver.SetActive(false);

    }
    public void Btn_GameOver()
    {

        MenuManager.Get.PanelGameOver.SetActive(false);
        ChangeState(GameState.MenuMain);
    }

    public void Btn_Start()
    {
        ChangeState(GameState.GameBegin);
    }
    public void Btn_Credit()
    {
        ChangeState(GameState.MenuCredit);
    }

    public void Btn_CreditBack()
    {
        ChangeState(GameState.MenuMain);
    }

    public void Btn_AppQuit()
    {
        Application.Quit();
    }

    public void Btn_LinkSite()
    {
        Application.OpenURL("http://makeunity.games/");
    }
    public void Btn_LinkIGDA()
    {
        Application.OpenURL("https://www.igda.org/");
    }
    public void Btn_LinkMark()
    {
        Application.OpenURL("http://www.soullessstudios.com/");
    }
    public void Btn_LinkJonai()
    {
        Application.OpenURL("http://www.chillwizardgames.com/");
    }
}
