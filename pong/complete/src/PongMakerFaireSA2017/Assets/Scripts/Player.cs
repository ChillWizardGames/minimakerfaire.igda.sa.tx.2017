﻿/*
///
    Player for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : User {

/*
///
    Here are the variables we are using to Model the Player.

    First we will go over some key terms

        Access Modifier - Used to tell different pieces of an app what can communicate

            const   - these are hard coded into the application; Used for things which shouldn't change.
            public  - Means that we can access these variables if there is a reference to this class.

        Date Type - Used to tell the difference between objects and variables

            int         - whole number integers ( 1, 2, 3 )
            float       - numbers with decimal points ( 3.14, 1.3, 4.2 )
            Text        - the default object that displays UI Text in Unity


    Now how do we use these pieces in Player.cs?

    BARRIER_TOP /
    BARRIER_BOTTOM  =   # these values are the world coordinates for the boundaries in our game.
                        #
                        # *const*   - they should NEVER change.
                        # *float*   - to have a finer range of control over where things should stop. ( Unity World Space default tracking uses *float* )
                        #
                        # The values here were found by moving the unbounded 
                        # game object to the area wanted for the boundary. 
                        # That number was then reversed for the inverse boundary.
                         

        



///
*/

    const float     BARRIER_TOP      =  3.75f;
    const float     BARRIER_BOTTOM   = -3.75f;

    public static Player Get;

	// Use this for initialization
	void Start () {
        
	}
	
    public IEnumerator MovementCheck()
    {
        bool running = true;
        while (running)
        {

            bool    _isPlayerMoving  = false;
            Vector2 _playerDirection = Vector2.up;
            float   _playerPosition  = this.transform.position.y;

            if (Input.GetKey(KeyCode.UpArrow))
            {
                Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if(_playerPosition < BARRIER_TOP )
                {
                    _isPlayerMoving = true;
                    _playerDirection = Vector2.up;
                }
            }

            if (Input.GetKey(KeyCode.DownArrow) )
            {
                Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (_playerPosition > BARRIER_BOTTOM )
                {
                    _isPlayerMoving = true;
                    _playerDirection = Vector2.down;
                }
               
            }

            if ( _isPlayerMoving == true )
            {
                this.transform.Translate(_playerDirection * Speed * Time.deltaTime);
            }

            yield return new WaitForSeconds(.014f);
        }
    }

    void Awake() { Get = this; }

}
