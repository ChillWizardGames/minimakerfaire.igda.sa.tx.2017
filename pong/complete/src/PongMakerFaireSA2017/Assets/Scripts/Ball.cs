﻿/*
///
    Ball for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
///

///
*/
public class Ball : MonoBehaviour {

    public static Ball Get;

    public Animator         Animate;
    public TrailRenderer    Trail;
    public Rigidbody2D      Physics;
    public SpriteRenderer   Sprite;
    public Text             TimerDisplay;
    public Image            TimerBackground;

    public int HitsMax;
    public int HitsTotal;
    public int HitsPaddle;
    public int HitsWall;

    public int x;
    public int y;
   
    public float MaxVelocity;
    public float Magnitude;

    public bool IsActive = false;

    void Awake ()
    {
        Get = this;
    }

/*
///
    Start is a default method called by Unity when this object is created.
    It is Used for initialization
///
*/
	void Start () {
       
        HitsTotal = 0;

    }

    /*
    ///
        Here we have the method 'MovementCheck' - this was called earlier with the 'StartCoroutine()' Method
    ///
    */
    public IEnumerator MovementCheck()
    {
     
        if ( StateHandler.Get.CurrentState == GameState.GameOver)
        {
            Reset();

        }  else
        {

            while (StateHandler.Get.CurrentState == GameState.GameRunning)
            {
                if (IsActive)
                    Physics.velocity = Vector2.ClampMagnitude(Physics.velocity, MaxVelocity);

                yield return new WaitForSeconds(.014f);
            }

        }
        
        
    }
	
    public IEnumerator SpawnTimer()
    {

        Reset();

        TimerDisplay.text = "3";

        TimerDisplay    .enabled = true;
        TimerBackground .enabled = true;

        for ( int timer = 3; timer >= 1; timer-- )
        {

            TimerDisplay.text = timer.ToString();

            yield return new WaitForSeconds(1);

        }

        TimerDisplay    .enabled = false;
        TimerBackground .enabled = false;

        SpawnBall();
        StopCoroutine(SpawnTimer());
    }

    public void Reset ()
    {
        Trail.Clear();
        IsActive = false;
        
        HitsPaddle  = 0;
        HitsWall    = 0;

        Physics.velocity = Vector2.zero;
        this.transform.position = Vector3.zero;

    }

    public void SpawnBall()
    {
        
        StopCoroutine(SpawnTimer());
        Sprite.enabled = true;

        int a = Random.Range(1,5);
        
        if(a == 1)
        {
            x = 1;
            y = 1;   
        }else if(a == 2)
        {
            x = -1;
            y = 1;
        }else if(a == 3)
        {
            x = -1;
            y = -1;
        }else
        {
            x = 1;
            y = -1;
        }

        MaxVelocity = 7;
        Physics.AddForceAtPosition(new Vector2(x,y) * Magnitude, new Vector2(Physics.transform.position.x, Physics.transform.position.y));

        Animate.speed = 1;
        
        IsActive = true;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            SoundManager.Get.Play(SoundManager.Get.FX_HitPaddle);

            HitsPaddle += 1;
            HitsWall = 0;

            if(HitsPaddle > 3)
            {
                FixPosition();
            }
        }
        if(col.gameObject.tag == "Wall")
        {
            SoundManager.Get.Play(SoundManager.Get.FX_HitWall);

            HitsWall += 1;
            HitsPaddle = 0;

            if(HitsWall > 3)
            {
                FixPosition();
            }
        }
      
    }

    void FixPosition()
    {

        if (UserManager.Get.Enemy.Speed > 6)
        {
            UserManager.Get.Enemy.Speed-= .5f;
        }

        StartCoroutine(SpawnTimer());
    }

}
