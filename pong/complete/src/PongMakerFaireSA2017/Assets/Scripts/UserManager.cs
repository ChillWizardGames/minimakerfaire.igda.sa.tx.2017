﻿/*
///
    User Manager for Pong Demo 
    at the 
    San Antonio 2017 MiniMakerFaire 
    IGDA presents Getting Started : Making Games in Unity
    
    Authors : Mark Solis & Jonai Alvarez
    http://soullessstudios.com/
    http://chillwizardgames.com/
///
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserManager : MonoBehaviour {

/*
///
    Here are the variables we are using to Model the *UserManager*

    First we will go over some key terms

        Access Modifier - # Used to tell different pieces of an app what can communicate

            static  - # Used to make a reference accessible from anywhere in your app.

            public  - # Means that we can access these variables if there is a reference to this class.

        Date Type - # Used to tell the difference between objects and variables

            Player                   -   # Built from the User Object but with Player Specific variables + Methods

            Enemy                    -   # Built from the User Object but with Enemy Specific variables + Methods

            List<user>               -   # A List of the User Objects

            Dictionary <User, int>   -   # A Collection of Key:Value Pairs where the key is a *User* object. 
                                         # If you provide a correct *User* object you get the values - the *Int* - in this case the scores


    Now how do we use these pieces in UserManage.cs?

            Player /
            Enemy     -   # Placed in the Inspector
                            # Used to reference and access the variables / methods of the Player and Enemy classes / objects

            List  /
            ByUser    -   # Used to keep a list of our user objects

///
*/

    public static UserManager Get;

    public Player Player;
    public Enemy Enemy;

    public List<User> List;
    public Dictionary<User, int> ByUser;

/*
///
   
    *Start* configures our User Manager Script

///
*/
	void Start () {

// Our List of User Objects is Created and populated with Player and enemy
        List = new List<User>();

        List.Add(Player);
        List.Add(Enemy);

// Our Dictionary *ByUser* is Created and Populated with the Player / Enemy Objects with their corresponding Scores
        ByUser = new Dictionary<User, int>();

        ByUser.Add(Player, Player.Score);
        ByUser.Add(Enemy, Enemy.Score);

	}

// Awake is used to set the 'Get'Variable making this class available throughtout the app
    void Awake() { Get = this; }
}
