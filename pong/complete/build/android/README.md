# README #
### What is this repository for? ###

This folder contains the *Android* version of the ***Pong*** app you can run.

### Where do I start? ###

***Download*** the pong.apk onto your... :

  * A.) Computer, and Install it onto your *Android* device, via a USB cable.
  * or B.) Directly to your *Android* device, ***Installing*** it, if you have *Installation from Unknown Sources Enabled*.

### About Unknown Sources ###

By default, *Android* won't let you install anything not found on the app store.

In general, it is *NOT ADVISED* to deactivate this setting.

To play our game, as it is not on the app store, you are going to need to deactivate it. :

  1. Go to *Settings*
  2. Navigate to *Security*
  3. *Disable* the box labeled *Unknown Sources*

# After you are done with the installation #
**Enable *Unknown Sources* Protection!**
