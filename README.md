# README #
### What is this repository for? ###

Making A Unity Game!
Brought to you by the Independent Game Developers Association in San Antonio.
Presenting at the San Antonio Central Public Library
during the miniMakerFaire on 3.25.17

### Speakers ###

Mark Solis
Jonai Alvarez

### How do I get set up? ###
Go to this link and download a version of Unity that is compatible with your Operation System
-> https://unity3d.com/get-unity/download